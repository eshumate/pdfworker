﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDFWorker
{
    public partial class PDFWorkerView : Form
    {
        public static PDFWorkerView Instance;

        public PDFWorkerView()
        {
            InitializeComponent();
            Instance = this;
        }

        private void btnPDFBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                Title = "Browse PDF File",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "PDF",
                Filter = "PDF files (*.PDF)|*.PDF",
                FilterIndex = 2,
                RestoreDirectory = true,
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtPDFPath.Text = openFileDialog1.FileName;
            }
        }

        private void btnOutputPathBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            if(!String.IsNullOrEmpty(txtPDFPath.Text))
                folderBrowser.SelectedPath = System.IO.Path.GetDirectoryName(txtWordTemplatePath.Text);
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                txtOutputPath.Text = folderBrowser.SelectedPath;
            }
        }

        private void btnWordTemplateBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                Title = "Browse Word File",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "docx",
                Filter = "Word Files (*.docx)|*.docx|Older Word Files (*.doc)|*.doc",
                FilterIndex = 1,
                RestoreDirectory = true,
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtWordTemplatePath.Text = openFileDialog1.FileName;
            }
        }

        private void btnSigImageBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                Title = "Browse Image Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "docx",
                Filter = "PNG Files (*.png)|*.png|JPEG Files (*.jpg)|*.jpg",
                FilterIndex = 1,
                RestoreDirectory = true,
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtSigImage.Text = openFileDialog1.FileName;
            }
        }

        private TestReport GetValues()
        {
            string pageText = "";
            TestReport report = new TestReport();

            using (var reader = new PdfReader(txtPDFPath.Text))
            {
                var fields = reader.AcroFields.Fields;

                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    pageText += PdfTextExtractor.GetTextFromPage(reader, i);
                }
            }

            report.SerialNumber = FindValueInString("Serial Number:", pageText, 0, false);
            report.JobNumber = FindValueInString("Job Number:", pageText, 0, false);
            report.Customer = FindValueInString("Customer:", pageText, 1, true);
            report.BreakerSerial = FindValueInString("Breaker Serial:", pageText, 0, true);
            report.TestDate = FindValueInString("Test Date:", pageText, 0, true);
            report.Technician = FindValueInString("Technician:", pageText, 1, true);
            report.Designation = FindValueInString("Designation:", pageText, 1, true);

            return report;
        }

        private string ReplaceInTemplate(TestReport report)
        {
            string tempWordDoc = System.IO.Path.GetDirectoryName(txtWordTemplatePath.Text) + "\\";
            int i = 0;
            while (File.Exists(tempWordDoc + "temp" + i + ".docx"))
                i++;
            tempWordDoc += "temp" + i + ".docx";
            File.Copy(txtWordTemplatePath.Text, tempWordDoc);

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(tempWordDoc);
            app.Visible = false;
            doc.Activate();
            FindAndReplace(app, "<SerialNumber>", report.SerialNumber);
            FindAndReplace(app, "<JobNumber>", report.JobNumber);
            FindAndReplace(app, "<Customer>", report.Customer);
            FindAndReplace(app, "<BreakerSerial>", report.BreakerSerial);
            FindAndReplace(app, "<TestDate>", report.TestDate);
            FindAndReplace(app, "<Technician>", report.Technician);
            FindAndReplace(app, "<Designation>", report.Designation);
            FindAndReplace(app, "<Initials>", report.Initials);

            //doc.Save();
            string wordDocPDF = System.IO.Path.GetDirectoryName(txtWordTemplatePath.Text) + "\\";
            int x = 0;
            while (File.Exists(wordDocPDF + "temp" + i + ".pdf"))
                x++;
            wordDocPDF += "temp" + i + ".pdf";
            doc.SaveAs2(wordDocPDF, WdSaveFormat.wdFormatPDF);
            doc.Close();
            app.Quit();

            File.Delete(tempWordDoc);

            return wordDocPDF;
        }

        private string FindValueInString(string needle, string haystack, int numberOfSpaces, bool endsInReturn)
        {
            int index;
            index = haystack.IndexOf(needle, 0) + needle.Length;
            var possibleValues = haystack.Substring(index, 20).Split(new char[] { '\n', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string value = "";
            for (int i = 0; i <= numberOfSpaces; i++)
            {
                if (value == "")
                    value += possibleValues[i];
                else
                    value += " " + possibleValues[i];
            }
            LogOutput(needle + " " + value); //Log
            return value;
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            if (!File.Exists(txtPDFPath.Text))
            {
                LogOutput("Invalid PDF Path");
                return;
            }
            if (!File.Exists(txtWordTemplatePath.Text))
            {
                LogOutput("Invalid Word Template Path");
                return;
            }
            if (!Directory.Exists(txtOutputPath.Text))
            {
                try
                {
                    Directory.CreateDirectory(txtOutputPath.Text);
                }
                catch
                {
                    LogOutput("Invalid Output Path");
                    return;
                }
            }
            if (!String.IsNullOrEmpty(txtSigImage.Text) && !File.Exists(txtSigImage.Text))
            {
                LogOutput("Invalid Signature Image Path");
                return;
            }
            //Getting Values
            TestReport report = GetValues();
            LogOutput("Got Values From Test PDF");
            //Replacing template keywords
            string wordDocPDF = ReplaceInTemplate(report);
            LogOutput("Saved Word Doc As Temp PDF");
            string tempPDF = System.IO.Path.GetDirectoryName(txtWordTemplatePath.Text) + "\\";
            int i = 0;
            while (File.Exists(tempPDF + "temp" + i + ".pdf"))
                i++;
            tempPDF += "temp" + i + ".pdf";
            //Sign PDF

            SignPDF(txtPDFPath.Text, tempPDF);
            LogOutput("Signed PDF and Saved As Temp PDF");
            var completePDF = System.IO.Path.GetFileNameWithoutExtension(txtPDFPath.Text);
            completePDF += "_COMPLETE.pdf";
            CreateMergedPDF(txtOutputPath.Text + "\\" + completePDF, tempPDF, wordDocPDF);
            LogOutput("Merged Temp PDFs and Generated Final PDF");
            if (File.Exists(wordDocPDF))
                File.Delete(wordDocPDF);
            if (File.Exists(tempPDF))
                File.Delete(tempPDF);
            LogOutput("Deleted Temp Files");
        }

        private void txtLog_TextChanged(object sender, EventArgs e)
        {
            txtLog.SelectionStart = txtLog.Text.Length;
            txtLog.ScrollToCaret();
        }

        public void LogOutput(string log)
        {
            txtLog.Text += log + Environment.NewLine;
        }

        private void SignPDF(string inputPDF, string outputPDF)
        {
            using (Stream inputPdfStream = new FileStream(inputPDF, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (Stream inputImageStream = new FileStream(txtSigImage.Text, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (Stream outputPdfStream = new FileStream(outputPDF, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                var reader = new PdfReader(inputPdfStream);
                var stamper = new PdfStamper(reader, outputPdfStream);
                var pdfContentByte = stamper.GetOverContent(1);

                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(inputImageStream);
                float xpos, ypos, width, height;
                try
                {
                    xpos = (float)Convert.ToDouble(txtSigXPos);
                    ypos = (float)Convert.ToDouble(txtSigXPos);
                    width = (float)Convert.ToDouble(txtSigXPos);
                    height = (float)Convert.ToDouble(txtSigXPos);
                }
                catch
                {
                    xpos = 140;
                    ypos = 90;
                    width = 100;
                    height = 50;
                }
                image.SetAbsolutePosition(140, 90);
                image.ScaleAbsoluteHeight(50);
                image.ScaleAbsoluteWidth(100);
                pdfContentByte.AddImage(image);
                stamper.Close();
            }
        }

        private void CreateMergedPDF(string outputPDF, string PDF1, string PDF2)
        {
            using (FileStream stream = new FileStream(outputPDF, FileMode.Create))
            {
                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4);
                PdfCopy pdf = new PdfCopy(pdfDoc, stream);
                pdfDoc.Open();
                var doc1 = new PdfReader(PDF1);
                var doc2 = new PdfReader(PDF2);
                pdf.AddDocument(doc1);
                pdf.AddDocument(doc2);
                if (pdfDoc != null)
                    pdfDoc.Close();
                if (doc1 != null)
                    doc1.Close();
                if (doc2 != null)
                    doc2.Close();
            }
        }

        private void FindAndReplace(Microsoft.Office.Interop.Word.Application WordApp, object findText, object replaceWithText)
        {
            object matchCase = true;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object nmatchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue;
            object replaceAll = Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll;
            WordApp.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord, ref matchWildCards, ref matchSoundsLike,
            ref nmatchAllWordForms, ref forward,
            ref wrap, ref format, ref replaceWithText,
            ref replaceAll, ref matchKashida,
            ref matchDiacritics, ref matchAlefHamza,
            ref matchControl);
        }

        private void txtSigImage_TextChanged(object sender, EventArgs e)
        {
            imgSignatureControl.ImageLocation = txtSigImage.Text;
        }


        //private void SignPDF()
        //{
        //    PdfReader reader = new PdfReader("original.pdf");
        //    Stream fout = new Stream("signed.pdf");
        //    PdfStamper stp = PdfStamper.createSignature(reader, fout, '\0');
        //    PdfSignatureAppearance sap = stp.SignatureAppearance;
        //    sap.setCrypto(key, chain, null, PdfSignatureAppearance.WINCER_SIGNED);
        //    sap.setReason("I'm the author");
        //    sap.setLocation("Lisbon");
        //    // comment next line to have an invisible signature
        //    sap.setVisibleSignature(new Rectangle(100, 100, 200, 200), 1, null);
        //    stp.close()
        //}
    }
}

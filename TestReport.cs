﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFWorker
{
    public class TestReport
    {
        private string _technichian;

        public string SerialNumber;
        public string JobNumber;
        public string Customer;
        public string BreakerSerial;
        public string TestDate;
        public string Technician
        {
            get { return _technichian; }
            set
            {
                _technichian = value;
                string[] fullname = _technichian.Split(' ');
                if(fullname.Length > 1)
                {
                    Initials = fullname[0].Substring(0, 1) + fullname[1].Substring(0, 1);
                    PDFWorkerView.Instance.LogOutput("Initials: " + Initials);
                }
            }
        }
        public string Designation;

        public string Initials;
    }
}

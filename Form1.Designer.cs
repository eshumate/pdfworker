﻿namespace PDFWorker
{
    partial class PDFWorkerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOutputPathBrowse = new System.Windows.Forms.Button();
            this.txtOutputPath = new System.Windows.Forms.TextBox();
            this.btnWordTemplateBrowse = new System.Windows.Forms.Button();
            this.txtWordTemplatePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPDFBrowse = new System.Windows.Forms.Button();
            this.txtPDFPath = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.btnConvert = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSigImageBrowse = new System.Windows.Forms.Button();
            this.txtSigImage = new System.Windows.Forms.TextBox();
            this.imgSignatureControl = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSigWidth = new System.Windows.Forms.TextBox();
            this.txtSigHeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSigYPos = new System.Windows.Forms.TextBox();
            this.txtSigXPos = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSignatureControl)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnOutputPathBrowse);
            this.groupBox1.Controls.Add(this.txtOutputPath);
            this.groupBox1.Controls.Add(this.btnWordTemplateBrowse);
            this.groupBox1.Controls.Add(this.txtWordTemplatePath);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnPDFBrowse);
            this.groupBox1.Controls.Add(this.txtPDFPath);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(385, 131);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Folder Paths";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Word Template File:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Output PDF Folder:";
            // 
            // btnOutputPathBrowse
            // 
            this.btnOutputPathBrowse.Location = new System.Drawing.Point(304, 92);
            this.btnOutputPathBrowse.Name = "btnOutputPathBrowse";
            this.btnOutputPathBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnOutputPathBrowse.TabIndex = 4;
            this.btnOutputPathBrowse.Text = "Browse";
            this.btnOutputPathBrowse.UseVisualStyleBackColor = true;
            this.btnOutputPathBrowse.Click += new System.EventHandler(this.btnOutputPathBrowse_Click);
            // 
            // txtOutputPath
            // 
            this.txtOutputPath.Location = new System.Drawing.Point(118, 95);
            this.txtOutputPath.Name = "txtOutputPath";
            this.txtOutputPath.Size = new System.Drawing.Size(169, 20);
            this.txtOutputPath.TabIndex = 3;
            // 
            // btnWordTemplateBrowse
            // 
            this.btnWordTemplateBrowse.Location = new System.Drawing.Point(304, 56);
            this.btnWordTemplateBrowse.Name = "btnWordTemplateBrowse";
            this.btnWordTemplateBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnWordTemplateBrowse.TabIndex = 4;
            this.btnWordTemplateBrowse.Text = "Browse";
            this.btnWordTemplateBrowse.UseVisualStyleBackColor = true;
            this.btnWordTemplateBrowse.Click += new System.EventHandler(this.btnWordTemplateBrowse_Click);
            // 
            // txtWordTemplatePath
            // 
            this.txtWordTemplatePath.Location = new System.Drawing.Point(118, 56);
            this.txtWordTemplatePath.Name = "txtWordTemplatePath";
            this.txtWordTemplatePath.Size = new System.Drawing.Size(169, 20);
            this.txtWordTemplatePath.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Test PDF File:";
            // 
            // btnPDFBrowse
            // 
            this.btnPDFBrowse.Location = new System.Drawing.Point(304, 20);
            this.btnPDFBrowse.Name = "btnPDFBrowse";
            this.btnPDFBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnPDFBrowse.TabIndex = 1;
            this.btnPDFBrowse.Text = "Browse";
            this.btnPDFBrowse.UseVisualStyleBackColor = true;
            this.btnPDFBrowse.Click += new System.EventHandler(this.btnPDFBrowse_Click);
            // 
            // txtPDFPath
            // 
            this.txtPDFPath.Location = new System.Drawing.Point(118, 20);
            this.txtPDFPath.Name = "txtPDFPath";
            this.txtPDFPath.Size = new System.Drawing.Size(169, 20);
            this.txtPDFPath.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.txtSigYPos);
            this.groupBox3.Controls.Add(this.txtSigXPos);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtSigHeight);
            this.groupBox3.Controls.Add(this.txtSigWidth);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.imgSignatureControl);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.btnSigImageBrowse);
            this.groupBox3.Controls.Add(this.txtSigImage);
            this.groupBox3.Location = new System.Drawing.Point(12, 149);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(385, 245);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Signature Information";
            // 
            // txtLog
            // 
            this.txtLog.AcceptsReturn = true;
            this.txtLog.AcceptsTab = true;
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtLog.Location = new System.Drawing.Point(12, 429);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(382, 176);
            this.txtLog.TabIndex = 4;
            this.txtLog.TextChanged += new System.EventHandler(this.txtLog_TextChanged);
            // 
            // btnConvert
            // 
            this.btnConvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvert.Location = new System.Drawing.Point(300, 400);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(97, 23);
            this.btnConvert.TabIndex = 5;
            this.btnConvert.Text = "Convert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Signature Image File:";
            // 
            // btnSigImageBrowse
            // 
            this.btnSigImageBrowse.Location = new System.Drawing.Point(304, 23);
            this.btnSigImageBrowse.Name = "btnSigImageBrowse";
            this.btnSigImageBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnSigImageBrowse.TabIndex = 4;
            this.btnSigImageBrowse.Text = "Browse";
            this.btnSigImageBrowse.UseVisualStyleBackColor = true;
            this.btnSigImageBrowse.Click += new System.EventHandler(this.btnSigImageBrowse_Click);
            // 
            // txtSigImage
            // 
            this.txtSigImage.Location = new System.Drawing.Point(118, 26);
            this.txtSigImage.Name = "txtSigImage";
            this.txtSigImage.Size = new System.Drawing.Size(169, 20);
            this.txtSigImage.TabIndex = 3;
            this.txtSigImage.TextChanged += new System.EventHandler(this.txtSigImage_TextChanged);
            // 
            // imgSignatureControl
            // 
            this.imgSignatureControl.Location = new System.Drawing.Point(14, 54);
            this.imgSignatureControl.Name = "imgSignatureControl";
            this.imgSignatureControl.Size = new System.Drawing.Size(362, 117);
            this.imgSignatureControl.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgSignatureControl.TabIndex = 6;
            this.imgSignatureControl.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 183);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Width: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Height:";
            // 
            // txtSigWidth
            // 
            this.txtSigWidth.Location = new System.Drawing.Point(61, 180);
            this.txtSigWidth.Name = "txtSigWidth";
            this.txtSigWidth.Size = new System.Drawing.Size(64, 20);
            this.txtSigWidth.TabIndex = 9;
            this.txtSigWidth.Text = "100";
            // 
            // txtSigHeight
            // 
            this.txtSigHeight.Location = new System.Drawing.Point(61, 206);
            this.txtSigHeight.Name = "txtSigHeight";
            this.txtSigHeight.Size = new System.Drawing.Size(64, 20);
            this.txtSigHeight.TabIndex = 10;
            this.txtSigHeight.Text = "50";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(198, 183);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "X Position:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(198, 208);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Y Position:";
            // 
            // txtSigYPos
            // 
            this.txtSigYPos.Location = new System.Drawing.Point(261, 208);
            this.txtSigYPos.Name = "txtSigYPos";
            this.txtSigYPos.Size = new System.Drawing.Size(64, 20);
            this.txtSigYPos.TabIndex = 14;
            this.txtSigYPos.Text = "90";
            // 
            // txtSigXPos
            // 
            this.txtSigXPos.Location = new System.Drawing.Point(261, 182);
            this.txtSigXPos.Name = "txtSigXPos";
            this.txtSigXPos.Size = new System.Drawing.Size(64, 20);
            this.txtSigXPos.TabIndex = 13;
            this.txtSigXPos.Text = "140";
            // 
            // PDFWorkerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 612);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "PDFWorkerView";
            this.Text = "PDFWorker";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSignatureControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPDFBrowse;
        private System.Windows.Forms.TextBox txtPDFPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnWordTemplateBrowse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOutputPathBrowse;
        private System.Windows.Forms.TextBox txtOutputPath;
        private System.Windows.Forms.TextBox txtWordTemplatePath;
        private System.Windows.Forms.PictureBox imgSignatureControl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSigImageBrowse;
        private System.Windows.Forms.TextBox txtSigImage;
        private System.Windows.Forms.TextBox txtSigYPos;
        private System.Windows.Forms.TextBox txtSigXPos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSigHeight;
        private System.Windows.Forms.TextBox txtSigWidth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}

